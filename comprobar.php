<?php

$tablas = $_POST['tabla'];
$ntablas = count($tablas);
$rellenadas = 0;
$vacios = 0;

//Cuento las tablas que se rellenaron.
for ($i = 0; $i < count($tablas); $i++) {
    if ($tablas[$i] != null || $tablas[$i] != '') {
        $rellenadas++;
    }
}

//Quito y cuento las tablas vacias.
for ($i = 0; $i < $ntablas; $i++) {
    if ($tablas[$i] == '') {
        unset($tablas[$i]);
        $vacios++;
    }
}

$totalTablas = count($tablas);
$tablasUnicas = array_unique($tablas);

//Comparo las tablas unicas con todas las tablas, si las tablas unicas son iguales al total entonces no hay ningun elemento repetido.
if ($totalTablas == count($tablasUnicas)) {
    $repetidas = false;
} else {
    $repetidas = true;
}


if ($repetidas) {
    echo 'Hay elementos repetidos. <br>';
} else {
    echo 'No hay elementos repetidos. <br>';
}
echo "Has rellenado $rellenadas cajas de un total de $ntablas.";
