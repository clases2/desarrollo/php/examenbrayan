<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Examen</title>
        <style>
            h1{
                text-align: center;
            }
            #principal{
                margin: 30px;
                padding: 30px;
                background-color: antiquewhite;
            }
            #paso2{
                border: 2px black solid;
                width: fit-content;
                background-color: antiquewhite;
                position: relative;
                top: 40px;
                left: 80px;
            }
            div{
                margin: 10px;
            }
            form{
                margin: 30px;
                padding-top: 30px;
                border: 1px black solid;
                display: flex;
                flex-direction: column;
                align-items: flex-start;
            }
            form p{
                border: 2px black solid;
                background-color: antiquewhite;
                position: relative;
                top: -58px;
                left: 35px;
            }
        </style>
    </head>
    <body>
        <h1>Ejercicio numero 1 del examen de php</h1>
        <div id="principal">
            <p>Escriba un numero (0 < numero <= 10) y dibujare una tabla de una columna de ese tamaño con cajas de texto en cada celda.</p>

            <?php
            if (isset($_POST['dibujar'])) { //Carga la pagina al presionar el boton Comprobar, mostrando tantas tablas como el numero introducido.
                include './tablas.php';
            } else if (isset($_POST['restablecer'])) { //Carga la pagina principal.
                include './formulario.php';
            } else if (isset($_POST['comprobar'])) { //Muestro los resultados.
                include './comprobar.php';
            } else { //Carga la pagina por primera vez.
                include './formulario.php';
            }
            ?>
        </div>
    </body>
</html>
